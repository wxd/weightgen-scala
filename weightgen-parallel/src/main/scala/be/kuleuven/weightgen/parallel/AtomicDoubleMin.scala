package be.kuleuven.weightgen.parallel

import com.google.common.util.concurrent.AtomicDouble

final class AtomicDoubleMin(initialValue: Double = 1.0) {
  private val min: AtomicDouble = new AtomicDouble(initialValue)

  def get(): Double =
    min.get()

  def getOrUpdateIfSmaller(newMin: Double): Double = {
    // http://stackoverflow.com/questions/9363419/greater-than-compare-and-swap
    while (true) {
      val currentMin = min.get()
      if (newMin >= currentMin)
        return currentMin
      if (min.compareAndSet(currentMin, newMin))
        return newMin
//    else
//      keep trying
    }

    1/0 // Should never be reached
  }
}
