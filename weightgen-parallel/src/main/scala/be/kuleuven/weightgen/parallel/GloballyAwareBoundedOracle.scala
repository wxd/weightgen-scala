package be.kuleuven.weightgen.parallel

import be.kuleuven.weightgen.oracle.{BoundedCount, BoundedOracle}
import be.kuleuven.weightgen.{ParityConstraint, Problem, WeightFunction}

trait GloballyAwareBoundedOracle[S, P <: Problem[S]] extends BoundedOracle[S,P] {
  protected val minTracker: AtomicDoubleMin

  // Cannot really decorate an oracle, because hooking up to the internal loop is needed
  override def verifiedMinWeight(w: Double): Double =
    minTracker.getOrUpdateIfSmaller(w)

  protected final def verifiedBoundedCount(bc: BoundedCount,
                                           pivot: Double,
                                           weight: WeightFunction[S]): BoundedCount = {
    val currentMinWeight = minTracker.get()
    if (currentMinWeight < bc.minWeight)
      bc.copy(
        tooLarge = bc.totalWeight > effectivePivot(pivot, weight, currentMinWeight),
        minWeight = currentMinWeight
      )
    else
      bc
  }

  override def enumerate(problem: P,
                         weight: WeightFunction[S],
                         pivot: Double,
                         initialMinWeight: Double,
                         xors: IndexedSeq[ParityConstraint],
                         newXor: Option[ParityConstraint]): (C, BoundedCount) = {
    val (s, bc) = super.enumerate(problem, weight, pivot, initialMinWeight, xors, newXor)
    (s, verifiedBoundedCount(bc, pivot, weight))
  }

  override def count(problem: P,
                     weight:
                     WeightFunction[S],
                     pivot: Double,
                     initialMinWeight: Double,
                     xors: IndexedSeq[ParityConstraint],
                     newXor: Option[ParityConstraint]): BoundedCount =
    verifiedBoundedCount(super.count(problem, weight, pivot, initialMinWeight, xors, newXor), pivot, weight)
}
