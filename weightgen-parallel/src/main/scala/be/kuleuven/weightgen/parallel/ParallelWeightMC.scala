package be.kuleuven.weightgen.parallel

import java.util.concurrent.{ExecutorService, LinkedBlockingQueue}

import be.kuleuven.weightgen.counting.BasicWeightMC
import be.kuleuven.weightgen.oracle.BoundedCounter
import be.kuleuven.weightgen.{Problem, WeightFunction, WeightMcConfiguration, WeightedModelCount}

import scala.util.Random

trait ParallelWeightMC[S, P <: Problem[S]] extends BasicWeightMC[S,P] {
  protected def executorService(): ExecutorService
  protected def threads(): Int

  protected def awareCounter(minTracker: AtomicDoubleMin): GloballyAwareBoundedOracle[S, P]
  protected def rndForThread(baseRnd: Random): Random

  override def independentCounts(problem: P,
                                 weight: WeightFunction[S],
                                 configuration: WeightMcConfiguration,
                                 counter: Option[BoundedCounter[S,P]] = None)
                                (rnd: Random): Iterator[WeightedModelCount] = {
    if (counter.nonEmpty) // TODO: is cake pattern/decorator for oracles possible?
      System.err.println("ParallelWeightMC ignores the `counter` argument")

    val minTracker = new AtomicDoubleMin(weight.initialMinWeight)
    @inline def singleCount0(counter: BoundedCounter[S,P], rnd0: Random) =
      singleCount(problem, weight, minTracker.get(), configuration, counter)(rnd0)

    val pool = executorService()
    val queue = new LinkedBlockingQueue[WeightedModelCount](configuration.iterations)
    for (_ <- 1 to threads())
      pool.submit(new Runnable {
        override def run(): Unit = {
          val localCounter = awareCounter(minTracker)
          val localRnd = rndForThread(rnd)
          Iterator.continually(singleCount0(localCounter, localRnd))
            .takeWhile(_ => !Thread.interrupted())
            .foreach(wmc => queue.put(wmc))
        }
      })

    new QueueConsumingExecutorControllingIterator[WeightedModelCount](queue, pool)
  }
}
