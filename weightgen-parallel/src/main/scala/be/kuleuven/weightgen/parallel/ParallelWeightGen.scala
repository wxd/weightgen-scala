package be.kuleuven.weightgen.parallel

import java.util.concurrent.{ExecutorService, LinkedBlockingQueue}

import be.kuleuven.weightgen._
import be.kuleuven.weightgen.oracle.BoundedSolver
import be.kuleuven.weightgen.sampling.BasicWeightGen

import scala.util.Random

trait ParallelWeightGen[S, P <: Problem[S]] extends BasicWeightGen[S,P] {
  protected def executorService(): ExecutorService
  protected def threads(): Int

  protected def awareSolver(minTracker: AtomicDoubleMin): GloballyAwareBoundedOracle[S, P]
  protected def rndForThread(baseRnd: Random): Random

  override protected def sampleLargeProblem(problem: P,
                                            w: WeightFunction[S],
                                            conf: WeightGenConfiguration,
                                            wmc: WeightedModelCount,
                                            solutionWeight: (S) => Double)
                                           (rnd: Random): Iterator[S] = {
    val loThresh = if (conf.isSpecialCase(wmc)) 0 else conf.loThresh
    val initialXorCount = conf.startIteration(wmc)

    val minTracker = new AtomicDoubleMin(wmc.minWeight)
    @inline def singleCell0(solver: BoundedSolver[S, P], rnd0: Random) =
      singleCell(problem, w, solver, loThresh, conf.hiThresh, minTracker.get(), initialXorCount)(rnd0)

    val pool = executorService()
    val queue = new LinkedBlockingQueue[S](100)
    for (_ <- 1 to threads())
      pool.submit(new Runnable {
        override def run(): Unit = {
          val localSolver = awareSolver(minTracker)
          val localRnd = rndForThread(rnd)
          Iterator.continually(singleCell0(localSolver, localRnd))
            .takeWhile(_ => !Thread.interrupted())
            .foreach {
              case (Some(s), bc) => sampleFromCell(s, bc.totalWeight, solutionWeight)(rnd).foreach(queue.put)
              case _ =>
            }
        }
      })

    new QueueConsumingExecutorControllingIterator[S](queue, pool)
  }
}
