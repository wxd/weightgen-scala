package be.kuleuven.weightgen

import java.util.concurrent.{BlockingQueue, ExecutorService}

import scala.collection.GenTraversableOnce

package object parallel {

  private[parallel] final class QueueConsumingExecutorControllingIterator[S](val queue: BlockingQueue[S],
                                                                             val executor: ExecutorService) extends Iterator[S] {
    override def hasNext: Boolean = true
    override def next(): S = queue.take()

    private def wrapWrapper[A](iterator: Iterator[A]) =
      new ShutdownIterator[A](iterator, executor)

    // Wrap results of methods that return new iterator objects, so that `executor` gets shut down once `!hasNext`
    override def slice(from: Int, until: Int): Iterator[S] = wrapWrapper(super.slice(from, until))
    override def map[B](f: (S) => B): Iterator[B] = wrapWrapper(super.map(f))
    override def flatMap[B](f: (S) => GenTraversableOnce[B]): Iterator[B] = wrapWrapper(super.flatMap(f))
    override def filter(p: (S) => Boolean): Iterator[S] = wrapWrapper(super.filter(p))
    override def collect[B](pf: PartialFunction[S, B]): Iterator[B] = wrapWrapper(super.collect(pf))
    override def scanLeft[B](z: B)(op: (B, S) => B): Iterator[B] = wrapWrapper(super.scanLeft(z)(op))
    override def takeWhile(p: (S) => Boolean): Iterator[S] = wrapWrapper(super.takeWhile(p))
    override def dropWhile(p: (S) => Boolean): Iterator[S] = wrapWrapper(super.dropWhile(p))
    override def zip[B](that: Iterator[B]): Iterator[(S, B)] = wrapWrapper(super.zip(that))
    override def zipWithIndex: Iterator[(S, Int)] = wrapWrapper(super.zipWithIndex)
    override def zipAll[B, A1 >: S, B1 >: B](that: Iterator[B], thisElem: A1, thatElem: B1): Iterator[(A1, B1)] = wrapWrapper(super.zipAll(that, thisElem, thatElem))
    override def padTo[A1 >: S](len: Int, elem: A1): Iterator[A1] = wrapWrapper(super.padTo(len, elem))
    override def patch[B >: S](from: Int, patchElems: Iterator[B], replaced: Int): Iterator[B] = wrapWrapper(super.patch(from, patchElems, replaced))

    // Unsupported methods (no good reason; probably not even hard to implement)
    override def partition(p: (S) => Boolean): (Iterator[S], Iterator[S]) = throw new UnsupportedOperationException
    override def span(p: (S) => Boolean): (Iterator[S], Iterator[S]) = throw new UnsupportedOperationException
    override def buffered: BufferedIterator[S] = throw new UnsupportedOperationException
    override def grouped[B >: S](size: Int): GroupedIterator[B] = throw new UnsupportedOperationException
    override def sliding[B >: S](size: Int, step: Int): GroupedIterator[B] = throw new UnsupportedOperationException
    override def duplicate: (Iterator[S], Iterator[S]) = throw new UnsupportedOperationException
  }

  // FIXME: abstraction leaks, example: `it.take(10); it.take(10)`
  private[parallel] final class ShutdownIterator[S](val base: Iterator[S],
                                                    val executor: ExecutorService) extends Iterator[S] {
    override def hasNext: Boolean = {
      val hn = base.hasNext
      if (!hn)
        executor.shutdownNow()

      hn
    }

    override def next(): S = base.next()
  }
}
