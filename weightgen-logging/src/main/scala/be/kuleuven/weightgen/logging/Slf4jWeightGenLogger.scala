package be.kuleuven.weightgen.logging

import be.kuleuven.weightgen.leapfrogging.LeapfroggingWeightGenLogger
import be.kuleuven.weightgen.{MaxExtraConstraints, ParityConstraint, WeightGenConfiguration, WeightedModelCount}
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

trait Slf4jWeightGenLogger extends LeapfroggingWeightGenLogger {
  protected val logger: Logger

  override def logConfiguration(configuration: WeightGenConfiguration,
                                wmc: WeightedModelCount): Unit = {
    logger.info( f"Parameters: tolerance=${configuration.tolerance}%.2f/${configuration.eps}%.2f/${configuration.pivot.toInt}%d, startIteration=${configuration.startIteration(wmc)}%d")
    logger.info( f" (practice) loThresh = ${if (configuration.isSpecialCase(wmc)) 0 else configuration.loThresh * wmc.maxWeight}%.1f, hiThresh = ${configuration.hiThresh * wmc.maxWeight}%.2f")
    logger.debug(f"   (theory) loThresh = ${configuration.loThresh}%.2f, hiThresh = ${configuration.hiThresh}%.2f")
  }

  override def logInnerIteration(xors: Seq[ParityConstraint],
                                 tooLarge: Boolean,
                                 tooSmall: Boolean,
                                 totalWeight: Double,
                                 solutionCount: Int): Unit = {
    if (!tooLarge && !tooSmall)
      logger.info(f"Cell weight $totalWeight%.4f just right with ${xors.size}%d XORs ($solutionCount%d solutions)")
    else
      logger.debug(f"Cell weight $totalWeight%.4f ${if (tooLarge) "too large" else tooSmall}%s with ${xors.size}%d XORs ($solutionCount%d solutions)")
  }

  override def logAddingXOR(xor: ParityConstraint): Unit =
    logger.trace(s"Adding a constraint: $xor")

  override def logFailure(unsatisfiable: Boolean,
                          tooSmall: Boolean,
                          totalWeight: Double,
                          solutionCount: Int,
                          xors: Seq[ParityConstraint]): Unit = {
    logger.error(
      if (unsatisfiable) f"Problem with ${xors.size}%d XORs was unsatisfiable"
      else if (tooSmall) f"Cell weight $totalWeight%.2f too small with ${xors.size}%d XORs ($solutionCount%d solutions)"
      else               f"Large cell even with $MaxExtraConstraints extra XORs: cell weight = $totalWeight%.2f ($solutionCount%d solutions)"
    )
  }

  override def logRemovingXOR(xor: ParityConstraint): Unit =
    logger.trace(s"Removing a constraint: $xor")
}

class BasicSlf4jWeightGenLogger(name: String) extends Slf4jWeightGenLogger {
  override protected val logger = Logger(LoggerFactory.getLogger(name))
}
