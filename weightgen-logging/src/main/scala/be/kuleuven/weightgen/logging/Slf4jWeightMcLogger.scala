package be.kuleuven.weightgen.logging

import be.kuleuven.weightgen.leapfrogging.LeapfroggingWeightMcLogger
import be.kuleuven.weightgen.{ParityConstraint, WMC, WeightMcConfiguration, WeightedModelCount}
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

trait Slf4jWeightMcLogger extends LeapfroggingWeightMcLogger {
  protected val logger: Logger

  override def logConfiguration(configuration: WeightMcConfiguration): Unit =
    logger.info(f"Parameters: confidence=${configuration.confidence}%.2f/${configuration.iterations}%d${if (configuration.loose) "" else "+"}, tolerance=${configuration.tolerance}%.2f/${configuration.pivot.toInt}%d")

  override def logInnerIteration(tooLarge: Boolean, totalWeight: Double, xorCount: Int, minWeight: Double): Unit = {
    val outcome = if (tooLarge) "too large" else "just right"
    logger.debug(f"Cell weight $totalWeight%.4f is $outcome%s with $xorCount%d XORs")
    logger.trace(f"Current min.weight = $minWeight%.4f")
  }

  override def logOuterIteration(iteration: Int, result: WeightedModelCount, totalIterations: Int): Unit = {
    logger.info(f"Iteration ${iteration+1}%d/$totalIterations%d: cell weight ${result.cellWeight}%.4f with ${result.hashCount}%d XORs")
    logger.trace(f"Current min.weight = ${result.minWeight}%.4f")
  }

  override def logUnsatisfiable(): Unit =
    logger.error("Problem is unsatisfiable")

  override def logRestart(xor: ParityConstraint): Unit =
    logger.trace(s"Restarting with 1 XOR: $xor")

  override def logRestart(xors: Seq[ParityConstraint]): Unit =
    logger.trace(s"Restarting with ${xors.size} XORs:\n${xors.mkString("\n")}")

  override def logAddingXOR(xor: ParityConstraint): Unit =
    logger.trace(s"Adding a constraint: $xor")

  override def logResult(wmc: WeightedModelCount): Unit =
    logger.info(f"Final estimate: 2^${wmc.hashCount} * ${wmc.cellWeight}%.2f = ${wmc.estimate}%6.3e (eps=${wmc.error}%.2f, min.weight=${wmc.minWeight}%.4f, max.weight=${wmc.maxWeight}%.4f")

  override def logRemovingXOR(xor: ParityConstraint): Unit =
    logger.trace(s"Removing a constraint: $xor")

  override def logFailedLeapUpward(): Unit =
    logger.debug("Removing a constraint resulted in too large a cell")

  override def logEstimationAttempt[R](wmcs: Array[WeightedModelCount],
                                       medianBounds: (WeightedModelCount, WeightedModelCount),
                                       functionBounds: (R, R),
                                       iteration: Int): Unit = {
    logger.debug(f"Estimating a function of median WMC at iteration ${iteration+1}%d: function - [${functionBounds._1}%s; ${functionBounds._2}%s], median - [${WMC.asFormula(medianBounds._1)}%s; ${WMC.asFormula(medianBounds._2)}%s]")
    logger.trace(s"WMC trace:\n${wmcs.view.filter(_ != null).mkString("\n")}")
  }

  override def logEstimationSuccess[R](finalEstimate: R, iteration: Int): Unit =
    logger.debug(f"Estimating a function of median WMC converged early, at iteration ${iteration+1}%d: value = $finalEstimate%s")
}

class BasicSlf4jWeightMcLogger(name: String) extends Slf4jWeightMcLogger {
  override protected val logger = Logger(LoggerFactory.getLogger(name))
}
