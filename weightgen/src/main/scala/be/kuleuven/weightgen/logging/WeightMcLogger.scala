package be.kuleuven.weightgen.logging

import be.kuleuven.weightgen.{ParityConstraint, WeightMcConfiguration, WeightedModelCount}

trait WeightMcLogger {
  def logConfiguration(configuration: WeightMcConfiguration): Unit

  def logInnerIteration(tooLarge: Boolean,
                        totalWeight: Double,
                        xorCount: Int,
                        minWeight: Double): Unit

  def logOuterIteration(iteration: Int,
                        result: WeightedModelCount,
                        totalIterations: Int): Unit

  def logUnsatisfiable(): Unit

  def logRestart(xor: ParityConstraint): Unit

  def logRestart(xors: Seq[ParityConstraint]): Unit

  def logAddingXOR(xor: ParityConstraint): Unit

  def logResult(wmc: WeightedModelCount): Unit

  def logEstimationAttempt[R](wmcs: Array[WeightedModelCount],
                              medianBounds: (WeightedModelCount, WeightedModelCount),
                              functionBounds: (R, R),
                              iteration: Int): Unit

  def logEstimationSuccess[R](finalEstimate: R, iteration: Int): Unit
}

object NoOpWeightMcLogger extends WeightMcLogger {
  @inline
  override final def logConfiguration(configuration: WeightMcConfiguration): Unit = ()

  @inline
  override final def logOuterIteration(iteration: Int, result: WeightedModelCount, totalIterations: Int): Unit = ()

  @inline
  override final def logResult(wmc: WeightedModelCount): Unit = ()

  @inline
  override final def logUnsatisfiable(): Unit = ()

  @inline
  override final def logAddingXOR(xor: ParityConstraint): Unit = ()

  @inline
  override final def logInnerIteration(tooLarge: Boolean,
                                       totalWeight: Double,
                                       xorCount: Int,
                                       minWeight: Double): Unit = ()

  @inline
  override final def logRestart(xor: ParityConstraint): Unit = ()

  @inline 
  override final def logRestart(xors: Seq[ParityConstraint]): Unit = ()

  @inline
  override def logEstimationAttempt[R](wmcs: Array[WeightedModelCount],
                                       medianBounds: (WeightedModelCount, WeightedModelCount),
                                       functionBounds: (R, R),
                                       iteration: Int): Unit = ()

  @inline
  override def logEstimationSuccess[R](finalEstimate: R, iteration: Int): Unit = ()
}


