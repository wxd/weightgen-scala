package be.kuleuven.weightgen.logging

import be.kuleuven.weightgen.{ParityConstraint, WeightGenConfiguration, WeightedModelCount}

trait WeightGenLogger {
  def logConfiguration(configuration: WeightGenConfiguration,
                       wmc: WeightedModelCount): Unit

  def logInnerIteration(xors: Seq[ParityConstraint],
                        tooLarge: Boolean,
                        tooSmall: Boolean,
                        totalWeight: Double,
                        solutionCount: Int): Unit

  def logAddingXOR(xor: ParityConstraint): Unit

  def logFailure(unsatisfiable: Boolean,
                 tooSmall: Boolean,
                 totalWeight: Double,
                 solutionCount: Int,
                 xors: Seq[ParityConstraint]): Unit
}

object NoOpWeightGenLogger extends WeightGenLogger {
  @inline
  override def logConfiguration(configuration: WeightGenConfiguration,
                                wmc: WeightedModelCount): Unit = {}

  @inline
  override def logFailure(unsatisfiable: Boolean,
                          tooSmall: Boolean,
                          totalWeight: Double,
                          solutionCount: Int,
                          xors: Seq[ParityConstraint]): Unit = {}

  @inline
  override def logAddingXOR(xor: ParityConstraint): Unit = {}

  @inline
  override def logInnerIteration(xors: Seq[ParityConstraint],
                                 tooLarge: Boolean,
                                 tooSmall: Boolean,
                                 totalWeight: Double,
                                 solutionCount: Int): Unit = {}
}


