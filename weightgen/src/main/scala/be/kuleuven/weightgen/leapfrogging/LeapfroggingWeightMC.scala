package be.kuleuven.weightgen.leapfrogging

import be.kuleuven.weightgen._
import be.kuleuven.weightgen.counting.BasicWeightMC
import be.kuleuven.weightgen.oracle.{BoundedCount, BoundedCounter}

import scala.annotation.tailrec
import scala.util.Random

trait LeapfroggingWeightMC[S, P <: Problem[S]] extends BasicWeightMC[S,P] {
  override protected val logger: LeapfroggingWeightMcLogger

  protected def iterationsBeforeLeapfrogging(problem: P,
                                             weight: WeightFunction[S],
                                             configuration: WeightMcConfiguration): Int

  override protected def singleCount(problem: P,
                                     weight: WeightFunction[S],
                                     initialMinWeight: Double,
                                     configuration: WeightMcConfiguration,
                                     counter: BoundedCounter[S, P],
                                     initialXors: IndexedSeq[ParityConstraint])
                                    (rnd: Random): WeightedModelCount = {
    def countInstance(xors: IndexedSeq[ParityConstraint],
                      newXor: Option[ParityConstraint],
                      minWeight: Double): BoundedCount =
      counter.count(problem, weight, configuration.pivot, minWeight, xors, newXor)

    @inline def wmc(cellWeight: Double, hashCount: Int, minWeight: Double) =
      WeightedModelCount(cellWeight, hashCount, error = configuration.tolerance,
        minWeight = minWeight, maxWeight = weight.theoreticalMaxWeight(minWeight))

    def removeOneXOR(xors: IndexedSeq[ParityConstraint]): (IndexedSeq[ParityConstraint], ParityConstraint) =
      // TODO: currently just removes the last one, which is random anyway; check validity
      (xors.drop(1), xors.head)

    @tailrec
    def countRec(xors: IndexedSeq[ParityConstraint],
                 newXor: Option[ParityConstraint],
                 minWeight: Double,
                 direction: LeapDirection,
                 previousBoundedCount: Option[BoundedCount]): WeightedModelCount = {
      val bc = countInstance(xors, newXor, minWeight)
      logger.logInnerIteration(bc.tooLarge, bc.totalWeight, xors.size, bc.minWeight)

      bc match {
        case BoundedCount(cnt, false, newMinWeight) if xors.isEmpty =>
          WMC.exact(cnt, minWeight = newMinWeight, maxWeight = weight.theoreticalMaxWeight(newMinWeight))
        case BoundedCount(0, _, _) if xors.isEmpty =>
          logger.logUnsatisfiable()
          WMC.empty
        case BoundedCount(0, _, _) =>
          val otherXors = IndexedSeq.fill(xors.size)(ParityConstraint.generateFor(problem)(rnd))
          if (otherXors.size == 1) logger.logRestart(otherXors.head) else logger.logRestart(otherXors)

          countRec(otherXors, None, minWeight, direction, None)
        case BoundedCount(cnt, isTooLarge@false, newMinWeight) =>
          direction match {
            case Down       =>
              wmc(cnt, xors.size, newMinWeight)
            case Up | Start =>
              val (fewerXors, removedXor) = removeOneXOR(xors)
              logger.logRemovingXOR(removedXor)

              countRec(fewerXors, None, newMinWeight, Up, Some(bc))
          }
        case BoundedCount(_, isTooLarge@true, newMinWeight) =>
          direction match {
            case Down | Start =>
              val newXor = ParityConstraint.generateFor(problem)(rnd)
              logger.logAddingXOR(newXor)

              countRec(xors :+ newXor, Some(newXor), newMinWeight, Down, None)
            case Up =>
              logger.logFailedLeapUpward()
              wmc(previousBoundedCount.get.totalWeight, xors.size + 1, newMinWeight)
          }
      }
    }

    countRec(initialXors, None, initialMinWeight, Start, None)
  }

  override def independentCounts(problem: P,
                                 weight: WeightFunction[S],
                                 configuration: WeightMcConfiguration,
                                 counter: Option[BoundedCounter[S, P]])
                                (rnd: Random): Iterator[WeightedModelCount] = {
    val counter0 = counter.getOrElse(defaultCounter(problem))
    @inline def singleCount0(xors: IndexedSeq[ParityConstraint], minWeight: Double) =
      singleCount(problem, weight, minWeight, configuration, counter0, xors)(rnd)
    @inline def generateXORs(n: Int) =
      IndexedSeq.fill(n)(ParityConstraint.generateFor(problem)(rnd))

    val leapfroggingStart = iterationsBeforeLeapfrogging(problem, weight, configuration) max 1
    Iterator.iterate((singleCount0(IndexedSeq.empty, weight.initialMinWeight), 0, Int.MaxValue)) {
      case (previousCount, 0, _) if previousCount.hashCount == 0 =>
        (previousCount, -1, -1)
      case (WeightedModelCount(_, xors, _, newMinWeight, _), i, minHashCount) =>
        val newIteration = i + 1
        val newMinHashCount = Math.min(xors, minHashCount)

        val xorsToGenerate = if (newIteration >= leapfroggingStart) newMinHashCount else 1
        (singleCount0(generateXORs(xorsToGenerate), newMinWeight), newIteration, newMinHashCount)
    }.takeWhile { case (wmc, index, _) => index == 0 || wmc.hashCount > 0 }
      .map { case (wmc, index, _) =>
        logger.logOuterIteration(index, wmc, configuration.iterations)
        wmc
      }
  }
}
