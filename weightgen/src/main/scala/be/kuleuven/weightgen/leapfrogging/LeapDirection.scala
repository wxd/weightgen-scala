package be.kuleuven.weightgen.leapfrogging

sealed trait LeapDirection
case object Up extends LeapDirection
case object Down extends LeapDirection
case object Start extends LeapDirection
