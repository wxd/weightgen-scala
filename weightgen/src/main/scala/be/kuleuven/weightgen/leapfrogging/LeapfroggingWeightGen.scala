package be.kuleuven.weightgen.leapfrogging

import java.lang.Math.max

import be.kuleuven.weightgen.ParityConstraint.generateFor
import be.kuleuven.weightgen._
import be.kuleuven.weightgen.oracle.{BoundedCount, BoundedSolver}
import be.kuleuven.weightgen.sampling.BasicWeightGen

import scala.annotation.tailrec
import scala.util.Random

trait LeapfroggingWeightGen[S, P <: Problem[S]] extends BasicWeightGen[S, P] {
  override protected val logger: LeapfroggingWeightGenLogger

  protected def initialExtraXors(problem: P,
                                 weight: WeightFunction[S],
                                 loThresh: Double,
                                 hiThresh: Double,
                                 initialXorCount: Int): Int

  override protected def singleCell(problem: P,
                                    weight: WeightFunction[S],
                                    solver: BoundedSolver[S, P],
                                    loThresh: Double,
                                    hiThresh: Double,
                                    initialMinWeight: Double,
                                    initialXorCount: Int)
                                   (rnd: Random): (Option[solver.C], BoundedCount) = {
    @inline
    def solveInstance(xors: IndexedSeq[ParityConstraint],
                      newXor: Option[ParityConstraint],
                      minWeight: Double): (solver.C, BoundedCount) =
      solver.enumerate(problem, weight, hiThresh, minWeight, xors, newXor)

    @inline
    def removeOneXOR(xors: IndexedSeq[ParityConstraint]): (IndexedSeq[ParityConstraint], ParityConstraint) =
    // TODO: currently just removes the last one, which is random anyway; check validity
      (xors.drop(1), xors.head)

    @tailrec
    def solveRec(xors: IndexedSeq[ParityConstraint],
                 newXor: Option[ParityConstraint],
                 minWeight: Double,
                 direction: LeapDirection): (Option[solver.C], BoundedCount) = {
      val (solutions, cnt) = solveInstance(xors, newXor, minWeight)
      val cellWeight = cnt.totalWeight

      val maxWeight = weight.theoreticalMaxWeight(cnt.minWeight)
      val tooSmall = cellWeight < loThresh * maxWeight
      val tooLarge = cnt.tooLarge
      logger.logInnerIteration(xors, tooLarge, tooSmall, cellWeight, solutions.size)

      @inline def fail() = {
        logger.logFailure(unsatisfiable = solutions.isEmpty, tooSmall, cellWeight, solutions.size, xors)
        (None, cnt)
      }

      val extraXorCount = xors.size - initialXorCount
      val possibleToAddXORs    = extraXorCount <= 3
      val possibleToRemoveXORs = extraXorCount >= 0

      if (!tooSmall && !tooLarge && solutions.nonEmpty) {
        (Some(solutions), cnt)
      } else if (tooLarge) {
        direction match {
          case Up           => fail()
          case Down | Start =>
            if (possibleToAddXORs) {
              val newXor = generateFor(problem)(rnd)
              logger.logAddingXOR(newXor)

              solveRec(xors :+ newXor, Some(newXor), cnt.minWeight, Down)
            } else {
              fail()
            }
        }
      } else if (tooSmall) {
        direction match {
          case Down       => fail()
          case Up | Start =>
            if (possibleToRemoveXORs) {
              val (fewerXors, removedXor) = removeOneXOR(xors)
              logger.logRemovingXOR(removedXor)

              solveRec(fewerXors, None, cnt.minWeight, Up)
            } else {
              fail()
            }
        }
      } else {
        fail() // TODO: probably redundant
      }
    }

    val initialExtraXorCount = max(0, initialExtraXors(problem, weight, loThresh, hiThresh, initialXorCount) min MaxExtraConstraints)
    val initialXors = IndexedSeq.fill(initialXorCount + initialExtraXorCount)(generateFor(problem)(rnd))
    solveRec(initialXors, None, initialMinWeight, Start)
  }
}
