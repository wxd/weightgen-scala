package be.kuleuven.weightgen.leapfrogging

import be.kuleuven.weightgen.logging.WeightMcLogger
import be.kuleuven.weightgen.{ParityConstraint, WeightMcConfiguration, WeightedModelCount}

trait LeapfroggingWeightMcLogger extends WeightMcLogger {
  def logRemovingXOR(xor: ParityConstraint): Unit

  def logFailedLeapUpward(): Unit
}

object NoOpLeapfroggingWeightMcLogger extends LeapfroggingWeightMcLogger {
  @inline
  override def logRemovingXOR(xor: ParityConstraint): Unit = ()

  @inline
  override def logFailedLeapUpward(): Unit = ()

  @inline
  override def logConfiguration(configuration: WeightMcConfiguration): Unit = ()

  @inline
  override def logInnerIteration(tooLarge: Boolean, totalWeight: Double, xorCount: Int, minWeight: Double): Unit = ()

  @inline
  override def logOuterIteration(iteration: Int, result: WeightedModelCount, totalIterations: Int): Unit = ()

  @inline
  override def logUnsatisfiable(): Unit = ()

  @inline
  override def logRestart(xor: ParityConstraint): Unit = ()

  @inline
  override def logRestart(xors: Seq[ParityConstraint]): Unit = ()

  @inline
  override def logAddingXOR(xor: ParityConstraint): Unit = ()

  @inline
  override def logResult(wmc: WeightedModelCount): Unit = ()

  @inline
  override def logEstimationAttempt[R](wmcs: Array[WeightedModelCount],
                                       medianBounds: (WeightedModelCount, WeightedModelCount),
                                       functionBounds: (R, R),
                                       iteration: Int): Unit = ()

  @inline
  override def logEstimationSuccess[R](finalEstimate: R, iteration: Int): Unit = ()
}
