package be.kuleuven.weightgen.leapfrogging

import be.kuleuven.weightgen.{ParityConstraint, WeightGenConfiguration, WeightedModelCount}
import be.kuleuven.weightgen.logging.WeightGenLogger

trait LeapfroggingWeightGenLogger extends WeightGenLogger {
  def logRemovingXOR(xor: ParityConstraint): Unit
}

object NoOpLeapfroggingWeightGenLogger extends LeapfroggingWeightGenLogger {
  @inline
  override def logRemovingXOR(xor: ParityConstraint): Unit = ()

  @inline
  override def logConfiguration(configuration: WeightGenConfiguration, wmc: WeightedModelCount): Unit = ()

  @inline
  override def logInnerIteration(xors: Seq[ParityConstraint], tooLarge: Boolean, tooSmall: Boolean, totalWeight: Double, solutionCount: Int): Unit = ()

  @inline
  override def logAddingXOR(xor: ParityConstraint): Unit = ()

  @inline
  override def logFailure(unsatisfiable: Boolean, tooSmall: Boolean, totalWeight: Double, solutionCount: Int, xors: Seq[ParityConstraint]): Unit = ()
}
