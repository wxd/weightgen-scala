package be.kuleuven

import java.lang.Math._

package object weightgen {
  private val inverseLn2 = 1.0 / log(2)

  def log2(x: Double) = log(x) * inverseLn2

  trait Problem[S] {
    def varCount: Int
    def varIndices: IndexedSeq[Int] = 0 until varCount
  }

  type CSP[S] = Problem[S]

  val MaxExtraConstraints = 3

  def median(numbers: TraversableOnce[Double]): Double = {
    val arr = numbers.toArray
    val n = arr.length
    scala.util.Sorting.quickSort(arr)

    if (n % 2 == 1) {
      arr(n / 2)
    } else {
      val m = n / 2.0
      (arr(m.floor.toInt) + arr(m.ceil.toInt)) / 2.0
    }
  }

  val NoOp = (a: Any) => ()
  val NoOp2 = (a: Any, b: Any) => ()

  @inline def emptyIterator[S]: Iterator[S] = Iterator.empty

  def differenceSmallerThan(epsilon: Double): (Double, Double) => Boolean =
    return { (d1: Double, d2: Double) => (d1 - d2).abs <= epsilon }
}
