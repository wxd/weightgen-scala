package be.kuleuven.weightgen.sampling

import be.kuleuven.weightgen._
import be.kuleuven.weightgen.logging.WeightGenLogger
import be.kuleuven.weightgen.oracle.{BoundedCount, BoundedSolver}

import scala.annotation.tailrec
import scala.util.Random

trait BasicWeightGen[S, P <: Problem[S]] extends WeightGen[S, P] {
  protected val logger: WeightGenLogger

  protected def defaultSolver(problem: P): BoundedSolver[S, P]

  override def sample(problem: P,
                      w: WeightFunction[S],
                      conf: WeightGenConfiguration,
                      wmc: WeightedModelCount,
                      solutionWeight: Option[S => Double] = None)
                     (rnd: Random): Iterator[S] = {
    logger.logConfiguration(conf, wmc)
    val solutionWeight0 = solutionWeight.getOrElse(w)

    wmc match {
      case WeightedModelCount(0, _, _, _, _) =>
        logger.logFailure(unsatisfiable = true, tooSmall = false, totalWeight = 0, solutionCount = 0, xors = Seq.empty)
        Iterator.empty
      case WeightedModelCount(exactWeight, 0, _, minWeight, maxWeight) if exactWeight <= conf.hiThresh * maxWeight =>
        val solver = defaultSolver(problem)
        val (allSolutions, _) = solver.enumerate(problem, w, conf.hiThresh * maxWeight, minWeight, IndexedSeq())

        logger.logInnerIteration(tooLarge = false, tooSmall = false, totalWeight = exactWeight, solutionCount = allSolutions.size, xors = Seq.empty)
        Iterator.continually(sampleFromCell(allSolutions, exactWeight, solutionWeight0)(rnd)).flatten
      case _ =>
        sampleLargeProblem(problem, w, conf, wmc, solutionWeight0)(rnd)
    }
  }

  protected def sampleLargeProblem(problem: P,
                                   w: WeightFunction[S],
                                   conf: WeightGenConfiguration,
                                   wmc: WeightedModelCount,
                                   solutionWeight: S => Double)
                                  (rnd: Random): Iterator[S] = {
    val solver = defaultSolver(problem)

    val initialXorCount = conf.startIteration(wmc)
    val loThresh = if (conf.isSpecialCase(wmc)) 0 else conf.loThresh

    Iterator.iterate((emptyIterator[S], wmc.minWeight)) { case (_, minWeight) =>
      val (solutions, bc) = singleCell(problem, w, solver, loThresh, conf.hiThresh, minWeight, initialXorCount)(rnd)
      val sampleIterator = solutions match {
        case Some(s) => sampleFromCell(s, bc.totalWeight, solutionWeight)(rnd)
        case None => Iterator.empty
      }
      (sampleIterator, bc.minWeight)
    }.drop(1).map(_._1).flatten
  }

  protected def singleCell(problem: P,
                           weight: WeightFunction[S],
                           solver: BoundedSolver[S, P],
                           loThresh: Double,
                           hiThresh: Double,
                           initialMinWeight: Double,
                           initialXorCount: Int)
                          (rnd: Random): (Option[solver.C], BoundedCount) = {
    def solveInstance(xors: IndexedSeq[ParityConstraint],
                      newXor: Option[ParityConstraint],
                      minWeight: Double): (solver.C, BoundedCount) =
      solver.enumerate(problem, weight, hiThresh, minWeight, xors, newXor)

    @tailrec
    def solveRec(xors: IndexedSeq[ParityConstraint],
                 newXor: Option[ParityConstraint],
                 xorsToAdd: Int,
                 minWeight: Double): (Option[solver.C], BoundedCount) = {
      val (solutions, cnt) = solveInstance(xors, newXor, minWeight)
      val cellWeight = cnt.totalWeight

      val maxWeight = weight.theoreticalMaxWeight(cnt.minWeight)
      val tooSmall = cellWeight < loThresh * maxWeight
      val tooLarge = cnt.tooLarge
      logger.logInnerIteration(xors, tooLarge, tooSmall, cellWeight, solutions.size)

      if (tooLarge && xorsToAdd > 0) {
        val newXor = ParityConstraint.generateFor(problem)(rnd)
        logger.logAddingXOR(newXor)

        solveRec(xors :+ newXor, Some(newXor), xorsToAdd - 1, cnt.minWeight)
      } else if (!tooSmall && !tooLarge && solutions.nonEmpty) {
        (Some(solutions), cnt)
      } else {
        logger.logFailure(unsatisfiable = solutions.isEmpty, tooSmall, cellWeight, solutions.size, xors)
        (None, cnt)
      }
    }

    val initialXors = IndexedSeq.fill(initialXorCount)(ParityConstraint.generateFor(problem)(rnd))
    solveRec(initialXors, None, xorsToAdd = MaxExtraConstraints, initialMinWeight)
  }

  protected def sampleFromCell[C <: Traversable[S]](cell: C, cellWeight: Double, weight: S => Double)
                                                   (rnd: Random): Iterator[S] = {
    val r = rnd.nextDouble() * cellWeight
    val sample = if (weight(cell.head) < r) {
      var gas = 0.0
      cell.toIterator.dropWhile { s => gas += weight(s); gas < r }.next()
    } else {
      cell.head
    }

    Iterator.single(sample)
  }
}