package be.kuleuven.weightgen.sampling

import be.kuleuven.weightgen._
import be.kuleuven.weightgen.counting.BasicWeightMC

import scala.util.Random

trait BasicWeightGenWrapper[S, P <: Problem[S]] extends WeightGenWrapper[S,P] {
  override protected val counter: BasicWeightMC[S, P]
  override protected val sampler: BasicWeightGen[S, P]

  override final def countThenSample(problem: P,
                                     weight: WeightFunction[S],
                                     configuration: WeightGenConfiguration,
                                     wmc: Either[WeightMcConfiguration, WeightedModelCount],
                                     solutionWeight: Option[(S) => Double],
                                     lazyCounting: Boolean)
                                    (rnd: Random): Iterator[S] =
    wmc match {
      case Right(wmc0) =>
        sample(problem, weight, configuration, wmc0, solutionWeight)(rnd)
      case Left(conf) if lazyCounting =>
        val count0: () => WeightedModelCount = { () =>
          counter.estimateWmcFunction(problem, weight, conf, configuration.startIteration)(rnd)._2
        }
        new Iterator[S] {
          private lazy val base = sample(problem, weight, configuration, count0(), solutionWeight)(rnd)

          override def hasNext: Boolean = base.hasNext
          override def next(): S = base.next()
        }
      case Left(conf) if !lazyCounting =>
        val wmc0 = counter.estimateWmcFunction(problem, weight, conf, configuration.startIteration)(rnd)._2
        sample(problem, weight, configuration, wmc0, solutionWeight)(rnd)
    }
}
