package be.kuleuven.weightgen

import scala.util.Random

trait WeightGen[S, P <: Problem[S]] {
  def sample(problem: P,
             weight: WeightFunction[S],
             configuration: WeightGenConfiguration,
             weightedModelCount: WeightedModelCount,
             solutionWeight: Option[S => Double] = None)
            (rnd: Random): Iterator[S]
}

trait WeightGenWrapper[S, P <: Problem[S]] extends WeightGen[S,P] with WeightMC[S,P] {
  protected val counter: WeightMC[S,P]
  protected val sampler: WeightGen[S,P]

  override final def count(problem: P, weight: WeightFunction[S], configuration: WeightMcConfiguration)(rnd: Random) =
    counter.count(problem, weight, configuration)(rnd)

  override final def sample(problem: P,
                            weight: WeightFunction[S],
                            configuration: WeightGenConfiguration,
                            weightedModelCount: WeightedModelCount,
                            solutionWeight: Option[(S) => Double])
                           (rnd: Random): Iterator[S] =
    sampler.sample(problem, weight, configuration, weightedModelCount, solutionWeight)(rnd)

  def countThenSample(problem: P,
                            weight: WeightFunction[S],
                            configuration: WeightGenConfiguration,
                            wmc: Either[WeightMcConfiguration, WeightedModelCount] = Left(WeightMcConfiguration.default),
                            solutionWeight: Option[(S) => Double],
                            lazyCounting: Boolean = true)
                           (rnd: Random): Iterator[S] =
    wmc match {
      case Right(wmc0) =>
        sample(problem, weight, configuration, wmc0, solutionWeight)(rnd)
      case Left(conf) if lazyCounting =>
        val count0: () => WeightedModelCount = { () => count(problem, weight, conf)(rnd) }
        new Iterator[S] {
          private lazy val base = sample(problem, weight, configuration, count0(), solutionWeight)(rnd)

          override def hasNext: Boolean = base.hasNext
          override def next(): S = base.next()
        }
      case Left(conf) if !lazyCounting =>
        val wmc0 = count(problem, weight, conf)(rnd)
        sample(problem, weight, configuration, wmc0, solutionWeight)(rnd)
    }
}
