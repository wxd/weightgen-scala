package be.kuleuven.weightgen

import scala.util.Random

final class ParityConstraint(coefficients: TraversableOnce[Int], val parity: Boolean) {
  val coefficientSet = coefficients.toSet

  def check(values: TraversableOnce[Int]): Boolean = {
    val setBits = values.count(coefficientSet.contains)
    (setBits % 2 == 1) == parity
  }

  def checkSingleton(index: Int): Boolean =
    coefficientSet.contains(index) == parity

  override def toString =
    s"XOR(${coefficientSet.view.toSeq.sorted.mkString(" ")}) = ${if (parity) 'T' else 'F'}"
}

object ParityConstraint {
  def apply(coefficients: TraversableOnce[Int], parity: Boolean) =
    new ParityConstraint(coefficients, parity)

  def generate(indices: TraversableOnce[Int])(rnd: Random): ParityConstraint =
    ParityConstraint(indices.filter(_ => rnd.nextBoolean()), rnd.nextBoolean())

  def generate[V](variables: TraversableOnce[V], varIndex: V => Int)
                 (rnd: Random): ParityConstraint =
    generate(variables.map(varIndex))(rnd)

  def generate(varCount: Int)(rnd: Random): ParityConstraint =
    generate(0 until varCount)(rnd)

  def generateFor(problem: Problem[_])(rnd: Random): ParityConstraint =
    generate(problem.varIndices)(rnd)
}
