package be.kuleuven.weightgen.oracle

import be.kuleuven.weightgen.{ParityConstraint, Problem, WeightFunction}

import scala.util.control.Breaks._

trait CallbackBasedEnumerator[S] {
  def enumerate(callback: S => Unit): Unit

  def cleanUp(): Unit = ()
}

trait CallbackBasedBoundedOracle[S, P <: Problem[S]] extends BoundedOracle[S,P] {
  protected def enumerator(problem: P,
                           xors: IndexedSeq[ParityConstraint],
                           newXor: Option[ParityConstraint]): CallbackBasedEnumerator[S]

  override protected def doEnumerate(problem: P,
                                     weight: WeightFunction[S],
                                     pivot: Double,
                                     initialMinWeight: Double,
                                     xors: IndexedSeq[ParityConstraint],
                                     newXor: Option[ParityConstraint])
                                    (onSolution: (S, Double) => Unit): BoundedCount = {
    var totalWeight = 0.0
    var minWeight = initialMinWeight
    var pivot0 = effectivePivot(pivot, weight, minWeight)

    val solutions = enumerator(problem, xors, newXor)
    breakable {
      solutions.enumerate { solution =>
        val w = weight(solution)
        totalWeight += w

        onSolution(solution, w)

        if (w < minWeight) {
          minWeight = verifiedMinWeight(w)
          pivot0 = effectivePivot(pivot, weight, minWeight)
        }

        if (totalWeight > pivot0) {
          break
        }
      }
    }
    solutions.cleanUp()

    BoundedCount(totalWeight, totalWeight > pivot0, minWeight)
  }
}
