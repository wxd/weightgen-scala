package be.kuleuven.weightgen.oracle

import be.kuleuven.weightgen.{NoOp2, ParityConstraint, Problem, WeightFunction}

import scala.collection.generic.Growable
import scala.collection.mutable.{ArrayBuffer, ListBuffer}

trait BoundedOracle[S, P <: Problem[S]] extends BoundedSolver[S,P] with BoundedCounter[S,P] {
  // http://docs.scala-lang.org/overviews/collections/concrete-mutable-collection-classes.html
  // http://docs.scala-lang.org/overviews/collections/performance-characteristics.html
  protected type B <: Growable[S]

  protected def emptyBuffer(problem: P,
                            weight: WeightFunction[S],
                            pivot: Double,
                            initialMinWeight: Double): B
  protected def convertBuffer(buffer: B): C

  protected def addToBuffer(buffer: B, solution: S): Unit
  protected def storeSolutionWeight(solution: S, weight: Double): S = solution // No-op by default

  protected def doEnumerate(problem: P,
                            weight: WeightFunction[S],
                            pivot: Double,
                            initialMinWeight: Double,
                            xors: IndexedSeq[ParityConstraint],
                            newXor: Option[ParityConstraint])
                           (onSolution: (S, Double) => Unit): BoundedCount
  
  override def enumerate(problem: P,
                         weight: WeightFunction[S],
                         pivot: Double,
                         initialMinWeight: Double,
                         xors: IndexedSeq[ParityConstraint],
                         newXor: Option[ParityConstraint]): (C, BoundedCount) = {
    val buf = emptyBuffer(problem, weight, pivot = pivot, initialMinWeight = initialMinWeight)
    val bc = doEnumerate(problem, weight, pivot, initialMinWeight, xors, newXor) { (s, w) =>
      addToBuffer(buf, storeSolutionWeight(s, w))
    }

    (convertBuffer(buf), bc)
  }

  override def count(problem: P,
                     weight: WeightFunction[S],
                     pivot: Double,
                     initialMinWeight: Double,
                     xors: IndexedSeq[ParityConstraint],
                     newXor: Option[ParityConstraint]): BoundedCount =
    doEnumerate(problem, weight, pivot, initialMinWeight, xors, newXor)(onSolution = NoOp2)
}

trait ListBufferBoundedOracle[S, P <: Problem[S]] { self: BoundedOracle[S, P] =>
  override protected final type B = ListBuffer[S]
  override type C = Seq[S]

  override protected final def emptyBuffer(p: P, w: WeightFunction[S], pivot: Double, min: Double) =
    new ListBuffer[S]()

  override protected final def addToBuffer(buffer: ListBuffer[S], solution: S): Unit =
    buffer prepend solution

  override protected def convertBuffer(buffer: ListBuffer[S]): Seq[S] =
    buffer
}

trait ArrayBufferBoundedOracle[S, P <: Problem[S]] { self: BoundedOracle[S, P] =>
  override protected final type B = ArrayBuffer[S]
  override type C = Seq[S]

  protected val initialSizeFactor: Int = 4

  override protected final def emptyBuffer(p: P, w: WeightFunction[S], pivot: Double, min: Double) =
    new ArrayBuffer[S]((initialSizeFactor * pivot / w.theoreticalMaxWeight(min)).toInt)

  override protected final def addToBuffer(buffer: ArrayBuffer[S], solution: S): Unit =
    buffer append solution

  override protected def convertBuffer(buffer: ArrayBuffer[S]): Seq[S] =
    buffer
}
