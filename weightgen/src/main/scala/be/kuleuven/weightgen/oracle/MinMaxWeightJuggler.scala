package be.kuleuven.weightgen.oracle

import be.kuleuven.weightgen.WeightFunction

trait MinMaxWeightJuggler[S] {
  @inline def verifiedMinWeight(candidate: Double): Double =
      candidate
  
  def effectivePivot(pivot: Double, weight: WeightFunction[S], minWeight: Double) =
    pivot * weight.theoreticalMaxWeight(minWeight)
}
