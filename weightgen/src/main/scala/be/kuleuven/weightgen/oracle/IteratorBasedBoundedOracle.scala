package be.kuleuven.weightgen.oracle

import be.kuleuven.weightgen.{ParityConstraint, Problem, WeightFunction}

trait IteratorBasedBoundedOracle[S, P <: Problem[S]] extends BoundedOracle[S,P] {
  protected type I <: Iterator[S]

  protected def solutionIterator(problem: P,
                                 xors: IndexedSeq[ParityConstraint],
                                 newXor: Option[ParityConstraint]): I

  protected def closeIterator(iterator: I): Unit

  override protected final def doEnumerate(problem: P,
                                           weight: WeightFunction[S],
                                           pivot: Double,
                                           initialMinWeight: Double,
                                           xors: IndexedSeq[ParityConstraint],
                                           newXor: Option[ParityConstraint])
                                          (onSolution: (S, Double) => Unit): BoundedCount = {
    var totalWeight = 0.0
    var minWeight = initialMinWeight
    var pivot0 = effectivePivot(pivot, weight, minWeight)

    val solutions = solutionIterator(problem, xors, newXor)
    solutions.takeWhile(_ => totalWeight <= pivot0).foreach { solution =>
      val w = weight(solution)
      totalWeight += w

      onSolution(solution, w)

      if (w < minWeight) {
        minWeight = verifiedMinWeight(w)
        pivot0 = effectivePivot(pivot, weight, minWeight)
      }
    }
    closeIterator(solutions)

    BoundedCount(totalWeight, totalWeight > pivot0, minWeight)
  }
}
