package be.kuleuven.weightgen.oracle

import be.kuleuven.weightgen.{ParityConstraint, Problem, WeightFunction}

final case class BoundedCount(totalWeight: Double, tooLarge: Boolean, minWeight: Double)

trait BoundedCounter[S, P <: Problem[S]] extends MinMaxWeightJuggler[S] {
  def count(problem: P,
            weight: WeightFunction[S],
            pivot: Double,
            minWeight: Double,
            xors: IndexedSeq[ParityConstraint],
            newXor: Option[ParityConstraint] = None): BoundedCount
}
