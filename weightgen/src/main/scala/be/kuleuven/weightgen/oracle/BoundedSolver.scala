package be.kuleuven.weightgen.oracle

import be.kuleuven.weightgen.{ParityConstraint, Problem, WeightFunction}

trait BoundedSolver[S, P <: Problem[S]] extends MinMaxWeightJuggler[S] {
  type C <: Traversable[S]

  def enumerate(problem: P,
                weight: WeightFunction[S],
                pivot: Double,
                initialMinWeight: Double,
                xors: IndexedSeq[ParityConstraint],
                newXor: Option[ParityConstraint] = None): (C, BoundedCount)
}
