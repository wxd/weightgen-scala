package be.kuleuven.weightgen

import scala.util.Random

final case class WeightedModelCount(cellWeight: Double,
                                    hashCount: Int,
                                    error: Double,
                                    minWeight: Double = 0,
                                    maxWeight: Double = 1) extends Ordered[WeightedModelCount] {
  def estimate: Double = java.lang.Math.scalb(cellWeight, hashCount)
  def lowerBound: Double = java.lang.Math.scalb(cellWeight / (1 + error), hashCount)
  def upperBound: Double = java.lang.Math.scalb(cellWeight * (1 + error), hashCount)

  override def compare(that: WeightedModelCount): Int =
    if (this.hashCount == that.hashCount)
      this.cellWeight compare that.cellWeight
    else if (this.hashCount > that.hashCount)
      (this.cellWeight * math.pow(2, this.hashCount - that.hashCount)) compare that.cellWeight
    else // if (this.hashCount < that.hashCount)
      this.cellWeight compare (that.cellWeight * math.pow(2, that.hashCount - this.hashCount))

  override def equals(other: Any): Boolean = other match {
    case that: WeightedModelCount => this.cellWeight == that.cellWeight && this.hashCount == that.hashCount
    case _ => false
  }

  override def hashCode(): Int =
    cellWeight.hashCode() + 31 * hashCount.hashCode()
}

object WMC {
  def apply(cellWeight: Double,
            hashCount: Int,
            error: Double,
            minWeight: Double = 0,
            maxWeight: Double = 1): WeightedModelCount =
    WeightedModelCount(cellWeight, hashCount, error, minWeight, maxWeight)

  def exact(totalWeight: Double, minWeight: Double, maxWeight: Double): WeightedModelCount =
    WeightedModelCount(totalWeight, hashCount = 0, error = 0, minWeight, maxWeight)

  def empty: WeightedModelCount =
    WeightedModelCount(cellWeight = 0, hashCount = 0, error = 0)

  def asFormula(wmc: WeightedModelCount): String =
    f"${wmc.cellWeight}%.2f * 2^${wmc.hashCount}%d"
}

trait WeightMC[S, P <: Problem[S]] {
  def count(problem: P, weight: WeightFunction[S], configuration: WeightMcConfiguration)
           (rnd: Random): WeightedModelCount
}
