package be.kuleuven.weightgen

trait WeightFunction[-S] extends ((S) => Double) {
  def weight(solution: S): Double
  final def apply(solution: S) = weight(solution)

  def initialMinWeight: Double
  def theoreticalMaxWeight(observedMinWeight: Double): Double
}

trait BoundedWeightFunction[-S] extends WeightFunction[S] {
  def tiltBound: Double

  override def initialMinWeight: Double =
    1 / tiltBound

  override def theoreticalMaxWeight(observedMinWeight: Double): Double =
    observedMinWeight * tiltBound
}

object Uniform extends BoundedWeightFunction[Any] {
  def over[S]: BoundedWeightFunction[S] = this

  @inline override def weight(solution: Any): Double = 1.0
  @inline override def tiltBound: Double = 1.0
  @inline override def initialMinWeight: Double = 1.0
  @inline override def theoreticalMaxWeight(observedMinWeight: Double): Double = 1.0

  override def toString(): String = "Uniform"
}
