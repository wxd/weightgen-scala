package be.kuleuven.weightgen

import java.lang.Math._

final case class WeightGenConfiguration(kappa: Double) {
  require(0 <= kappa && kappa < 1, s"kappa=$kappa, must be in [0,1)")

  val eps: Double = (1 + kappa) * (2.36 + 0.51 / ((1 - kappa) * (1 - kappa))) - 1

  val pivot: Double = ceil(4.03 * (1 + 1 / kappa) * (1 + 1 / kappa))
  val loThresh: Double = pivot / (1.4143 * (1 + kappa))
  val hiThresh: Double = 1.4143 * (1 + kappa) * pivot + 1

  @inline def tolerance: Double = kappa

  // TODO: (1) What up with `maxWeight^-1`? (2) compare `wmc` and `thresh`es
  def startIteration(wmc: WeightedModelCount): Int = {
    val m = wmc.hashCount + log2(wmc.cellWeight) - log2(wmc.maxWeight) +
      log2(1 + wmc.error) - log2(pivot)
    max(ceil(m).toInt - 3, 0)
  }

  // TODO: Why log of product?
  def isSpecialCase(wmc: WeightedModelCount): Boolean =
    wmc.hashCount + log2(wmc.cellWeight) - log2(wmc.maxWeight) <= log2(1.4143 * (1 + kappa) * pivot)

  override def toString = f"WeightGenConf(tolerance=$eps%.2f/$kappa%.4f)"
}
