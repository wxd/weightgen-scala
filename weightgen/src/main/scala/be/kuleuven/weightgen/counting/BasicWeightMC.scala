package be.kuleuven.weightgen.counting

import be.kuleuven.weightgen._
import be.kuleuven.weightgen.logging.WeightMcLogger
import be.kuleuven.weightgen.oracle.{BoundedCount, BoundedCounter}

import scala.annotation.tailrec
import scala.util.Random

trait BasicWeightMC[S, P <: Problem[S]] extends WeightMC[S,P] {
  protected val logger: WeightMcLogger

  protected def defaultCounter(problem: P): BoundedCounter[S,P]

  protected def singleCount(problem: P,
                            weight: WeightFunction[S],
                            initialMinWeight: Double,
                            configuration: WeightMcConfiguration,
                            counter: BoundedCounter[S, P],
                            initialXors: IndexedSeq[ParityConstraint] = IndexedSeq.empty)
                           (rnd: Random): WeightedModelCount = {
    def countInstance(xors: IndexedSeq[ParityConstraint],
                      newXor: Option[ParityConstraint],
                      minWeight: Double): BoundedCount = {
      counter.count(problem, weight, configuration.pivot, minWeight, xors, newXor)
    }

    @tailrec
    def countRec(xors: IndexedSeq[ParityConstraint],
                 newXor: Option[ParityConstraint],
                 minWeight: Double): WeightedModelCount = {
      val bc = countInstance(xors, newXor, minWeight)
      logger.logInnerIteration(bc.tooLarge, bc.totalWeight, xors.size, bc.minWeight)

      bc match {
        case BoundedCount(cnt, false, newMinWeight) if xors.isEmpty =>
          // "Small problem" (WMC < pivot)
          WMC.exact(cnt, minWeight = newMinWeight, maxWeight = weight.theoreticalMaxWeight(newMinWeight))
        case BoundedCount(0, _, _) if xors.isEmpty =>
          // Unsatisfiable
          logger.logUnsatisfiable()
          WMC.empty
        case BoundedCount(0, _, _) =>
          // Unsatisfiable with XORs, need to restart
          val otherXors = IndexedSeq.fill(xors.size)(ParityConstraint.generateFor(problem)(rnd))
          if (otherXors.size == 1) logger.logRestart(otherXors.head) else logger.logRestart(otherXors)

          countRec(otherXors, None, minWeight)
        case BoundedCount(cnt, false, newMinWeight) =>
          // Right-sized cell
          WeightedModelCount(cnt, xors.size, error = configuration.tolerance,
            minWeight = newMinWeight, maxWeight = weight.theoreticalMaxWeight(newMinWeight))
        case BoundedCount(_, true, newMinWeight) =>
          // Too large a cell, adding an XOR
          val newXor = ParityConstraint.generateFor(problem)(rnd)
          logger.logAddingXOR(newXor)

          countRec(xors :+ newXor, Some(newXor), newMinWeight)
      }
    }

    countRec(initialXors, None, initialMinWeight)
  }

  def independentCounts(problem: P,
                        weight: WeightFunction[S],
                        configuration: WeightMcConfiguration,
                        counter: Option[BoundedCounter[S,P]] = None)
                       (rnd: Random): Iterator[WeightedModelCount] = {
    val counter0 = counter.getOrElse(defaultCounter(problem))
    @inline def singleCount0(xors: IndexedSeq[ParityConstraint], minWeight: Double) =
      singleCount(problem, weight, minWeight, configuration, counter0, xors)(rnd)

    Iterator.iterate(singleCount0(IndexedSeq.empty, weight.initialMinWeight)) {
      case previousCount@WeightedModelCount(_, 0, _, _, _) =>
        // No constraints were added (problem is either unsatisfiable, or small)
        previousCount
      case WeightedModelCount(_, xors, _, newMinWeight, _) if xors > 0 =>
        // As there were at least two cells in the previous iteration, start already with one XOR
        val xors = IndexedSeq(ParityConstraint.generateFor(problem)(rnd))
        singleCount0(xors, newMinWeight)
    }.zipWithIndex
      // Always return the first count; stop if there was no need to add constraints
      .takeWhile { case (wmc, index) => index == 0 || wmc.hashCount > 0 }
      .map { case (wmc, index) =>
        logger.logOuterIteration(index, wmc, configuration.iterations)
        wmc
      }

  }

  override def count(problem: P,
                     weight: WeightFunction[S],
                     configuration: WeightMcConfiguration)
                    (rnd: Random): WeightedModelCount = {
    logger.logConfiguration(configuration)

    independentCounts(problem, weight, configuration)(rnd).take(configuration.iterations).toIndexedSeq match {
      case IndexedSeq(count) =>
        // A single count implies that the problem is either unsatisfiable, or small TODO: might be false actually
        logger.logResult(count)
        count
      case counts =>
        val minWeight = counts.last.minWeight
        val maxWeight = weight.theoreticalMaxWeight(minWeight)
        val minHashCount = counts.view.map(_.hashCount).min

        @inline def adjustCellWeight(wmc: WeightedModelCount): Double =
          wmc.cellWeight * Math.pow(2, wmc.hashCount - minHashCount) / maxWeight // TODO: suspicious division

        val result = WeightedModelCount(
          cellWeight = median(counts.map(adjustCellWeight)),
          hashCount = minHashCount,
          error = configuration.tolerance,
          minWeight = minWeight,
          maxWeight = maxWeight
        )
        logger.logResult(result)

        result
    }
  }

  // FIXME: doesn't really work for even iterations
  // FIXME: most likely fails for "small problems"
  final def estimateWmcFunction[R](problem: P,
                                   weight: WeightFunction[S],
                                   configuration: WeightMcConfiguration,
                                   func: WeightedModelCount => R,
                                   areIdentical: (R, R) => Boolean = { (f1: R, f2: R) => f1 == f2 },
                                   returnUpperBound: Boolean = true)
                                  (rnd: Random): (R, WeightedModelCount) = {
    logger.logConfiguration(configuration)

    val wmcs = Array.ofDim[WeightedModelCount](configuration.iterations)
    def insert(wmc: WeightedModelCount): Unit = {
      var i: Int = 0
      var cur: WeightedModelCount = wmc
      while (wmcs(i) != null) {
        if (wmcs(i).compare(cur) > 0) {
          val buf = wmcs(i)
          wmcs(i) = cur
          cur = buf
        }
        i += 1
      }
      wmcs(i) = cur
    }

    val medIndex = (configuration.iterations / 2.0).floor.toInt
    @inline def medianBounds(iteration: Int) =
      (wmcs(iteration - medIndex), wmcs(medIndex))

    val unavoidableIterations = (configuration.iterations / 2.0).ceil.toInt
    independentCounts(problem, weight, configuration)(rnd)
      .take(configuration.iterations)
      .zipWithIndex
      .map { case (wmc, i) =>
        insert(wmc)

        if (i == configuration.iterations - 1) {
          val median = wmcs(medIndex)
          Some((func(median), median))
        } else if (i >= unavoidableIterations - 1) {
          val (medianLowerBound, medianUpperBound) = medianBounds(i)
          val funcLowerBound = func(medianLowerBound)
          val funcUpperBound = func(medianUpperBound)
          logger.logEstimationAttempt(wmcs, (medianLowerBound, medianUpperBound), (funcLowerBound, funcUpperBound), i)

          if (areIdentical(funcLowerBound, funcUpperBound)) {
            val finalEstimate = if (returnUpperBound)
              (funcUpperBound, medianUpperBound)
            else
              (funcLowerBound, medianLowerBound)
            logger.logEstimationSuccess(finalEstimate._1, i)

            Some(finalEstimate)
          } else {
            None
          }
        } else {
          None
        }
      }.find(_.nonEmpty).get.get
  }
}
