package be.kuleuven.weightgen

import java.lang.Math._

trait WeightMcConfiguration {
  val confidence: Double
  val tolerance: Double
  val loose: Boolean

  val iterations: Int
  val pivot: Double

  final override def toString =
    f"WeightMcConf${if (loose) 1 else 0}(confidence=$confidence%.3f/$iterations%d,tolerance=$tolerance%.3f/$pivot%.1f)"
}

final case class ArbitraryWeightMcConfiguration(iterations: Int,
                                                pivot: Double,
                                                loose: Boolean = true) extends WeightMcConfiguration {
  val tolerance = 1 / (sqrt(pivot / (6 * sqrt(E))) - 1)
  val confidence = if (loose)
    FormalWeightMcConfiguration.confidence(iterations)
  else
    1 - pow(2, log2(3) - iterations / 35.0)
}

final case class FormalWeightMcConfiguration(confidence: Double,
                                             tolerance: Double,
                                             loose: Boolean = true) extends WeightMcConfiguration {
  val pivot = {
    val tolTerm = 1 + 1.0 / tolerance
    2 * (FormalWeightMcConfiguration.sqrtE3 * tolTerm * tolTerm).ceil
  }
  val iterations = if (loose)
    FormalWeightMcConfiguration.iterations(confidence)
  else
    (35 * log2(3.0 / (1 - confidence))).ceil.toInt
}

object FormalWeightMcConfiguration {
  protected val sqrtE3 = sqrt(E * E * E)

  // www.cs.rice.edu/CS/Verification/Projects/ApproxMC/SourceCode/ApproxMC/ProbMapFile.txt
  // (only odd lines, up to 4 decimal points)
  private val precomputed = Seq(
    (0.6000, 1),
    (0.6480, 3),
    (0.6826, 5),
    (0.7102, 7),
    (0.7334, 9),
    (0.7535, 11),
    (0.7712, 13),
    (0.7869, 15),
    (0.8011, 17),
    (0.8139, 19),
    (0.8256, 21),
    (0.8364, 23),
    (0.8462, 25),
    (0.8553, 27),
    (0.8638, 29),
    (0.8716, 31),
    (0.8857, 35),
    (0.8920, 37),
    (0.9035, 41),
    (0.9135, 45),
    (0.9224, 49),
    (0.9303, 53),
    (0.9404, 59),
    (0.9515, 67),
    (0.9604, 75),
    (0.9706, 87),
    (0.9801, 103),
    (0.9902, 133)
  )

  def iterations(confidence: Double): Int =
    precomputed.view.dropWhile(_._1 < confidence)
      .headOption.getOrElse(precomputed.last)._2

  def confidence(iterations: Int): Double =
    precomputed.view.takeWhile(_._2 <= iterations)
      .lastOption.getOrElse(precomputed.head)._1
}

object WeightMcConfiguration {
  val defaultConfidence: Double = 0.8
  val defaultTolerance: Double = 0.8

  def apply(confidence: Double = defaultConfidence, tolerance: Double = defaultTolerance) =
    FormalWeightMcConfiguration(confidence, tolerance, loose = true)

  val default = WeightMcConfiguration()
}